import {Component} from '@angular/core';
import {RouterModule} from '@angular/router';
import {StellarAeroDropdown} from './stellar-aero-dropdown/stellar-aero-dropdown';

@Component({
  selector: 'fountain-root',
  template: '<router-outlet></router-outlet>'
})
export class RootComponent {}

export const routes = [
  {
    path: '',
    component: StellarAeroDropdown
  }
];

export const routing = RouterModule.forRoot(routes);
