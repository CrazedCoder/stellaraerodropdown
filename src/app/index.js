import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing, RootComponent} from './routes';

import {StellarAeroDropdown} from './stellar-aero-dropdown/stellar-aero-dropdown';

@NgModule({
  imports: [
    BrowserModule,
    routing
  ],
  declarations: [
    RootComponent,
    StellarAeroDropdown
  ],
  bootstrap: [RootComponent]
})
export class AppModule {}
