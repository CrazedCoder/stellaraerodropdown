import {Component, HostListener} from '@angular/core';

@Component({
  selector: 'stellar-aero-dropdown',
  template: require('./stellar-aero-dropdown.html')
})
export class StellarAeroDropdown {
  constructor() {
    this.isOpened = true;
    this.options = [{value: 'val1'}, {value: 'val2'}, {value: 'val3'}];
    this.value = this.options[0].value;
  }
  @HostListener('document:click', ['$event']) onOutsideClick() {
    // TODO; Provide elementref to identify self click
    // if (!this._eref.nativeElement.contains($event.target)) {
    this.isOpened = false;
    // }
  }
}
